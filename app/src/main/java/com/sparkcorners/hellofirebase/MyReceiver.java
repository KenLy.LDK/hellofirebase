package com.sparkcorners.hellofirebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    private MainActivity activity;

    public MyReceiver(MainActivity _activity) {
        this.activity = _activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            final TextView mShowLevel = this.activity.findViewById(R.id.show_level);
            if (Integer.parseInt(mShowLevel.getText().toString()) == 2) {
                String topup = intent.getStringExtra("topup");
                Toast.makeText(context, topup, Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new MyRunnable(this.activity) {
                    @Override
                    public void run() {
                        mShowLevel.setText(Integer.toString(3));
                        this.activity.fire_level_event(3);
                    }
                }, 2500);   // The delay here is on-purpose, just to show the effect in the demo

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class MyRunnable implements Runnable {
    public MainActivity activity;

    public MyRunnable(MainActivity _activity) {
        this.activity = _activity;
    }

    public void run() {
    }
}