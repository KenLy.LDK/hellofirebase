package com.sparkcorners.hellofirebase;

import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainActivity extends AppCompatActivity {
    private int mLevel = 0;
    private TextView mShowLevel;
    private FirebaseAnalytics mFirebaseAnalytics;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // This is the variable that holds the current level of the game
        mShowLevel = findViewById(R.id.show_level);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

    }

    MyReceiver receiver = new MyReceiver(this);

    @Override
    public void onStart() {
        super.onStart();
        registerReceiver(receiver, new IntentFilter("FCM"));
    }

    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(receiver);

    }


    public void fire_level_event(int level) {
        Bundle params = new Bundle();
        params.putInt("Level", level);
        mFirebaseAnalytics.logEvent("current_level", params);
    }


    /*
     * =======================
     * MAIN INTERFACE BUTTONS
     * =======================
     */

    // Reset the game to the beginning - Level 0
    public void reset(View view) {
        mLevel = 0;
        if (mShowLevel != null)
            mShowLevel.setText(Integer.toString(mLevel));

        fire_level_event(0);
    }

    // Increase the game counter - Level 1
    // - Only possible when the game is at Level 0
    public void level_1(View view) {
        if (Integer.parseInt(mShowLevel.getText().toString()) == 0) {
            mLevel = 1;
            if (mShowLevel != null)
                mShowLevel.setText(Integer.toString(mLevel));

            fire_level_event(1);
        }

    }

    // Increase the game counter - Level 2
    // - Only possible when the game is at Level 1
    public void level_2(View view) {
        if (Integer.parseInt(mShowLevel.getText().toString()) == 1) {
            mLevel = 2;
            if (mShowLevel != null)
                mShowLevel.setText(Integer.toString(mLevel));

            fire_level_event(2);
        }
    }

    //
    // Increase the game counter to Level 3
    // - Only possible after receiving the external event
    //

    // Increase the game counter - Level 4
    // - Only possible when the game is at Level 3
    public void level_4(View view) {
        if (Integer.parseInt(mShowLevel.getText().toString()) == 3) {
            mLevel = 4;
            if (mShowLevel != null)
                mShowLevel.setText(Integer.toString(mLevel));

            fire_level_event(4);
        }
    }

    // Increase the game counter - Level 5
    // - Only possible when the game is at Level 4
    public void level_5(View view) {
        if (Integer.parseInt(mShowLevel.getText().toString()) == 4) {
            mLevel = 5;
            if (mShowLevel != null)
                mShowLevel.setText(Integer.toString(mLevel));

            fire_level_event(5);
        }
    }

    // Retrieve the FCM token of this device
    public void get_fcm_token(View view) {
        // Get token
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]
    }
}
